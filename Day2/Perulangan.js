let n  = require ('readline-sync')
let jumlah = n.questionInt("JUmlah mahasiswa")

//Looping FOR (counted loop) = Perulangan yang memiliki batas tertentu
for (let i= 1; i<=jumlah; i++) {
    console.log("ini Pengulangan ke ",i," ya Bro")
}

// Looping While (uncounted Loop) = perulangan yang dilakukan sampai di kondisi tertentu
let i = 1
while (i<= jumlah) {
    console.log("mahasiswa ke ", i," Ya Bro")
    i++
}

do{
    console.log("mahasiswa ke ", i," Ya Bro")
i++// sama kayak repeat until di pascal
}while(i<=jumlah)

//nested LOOP = Pengulangan dalam perulangan
for(let i = 0; i < jumlah; i++){
    for(let j=0; j<jumlah; j++){
        console.log(i,j)
    }
}